# Deployment of the RAN part of the MEP platform 

This README will walk you through the steps to deploy the RAN part of the MEP platform, to do so 3 components will need to be up and running: 
- The 5G modem of OpenAirInterface : that represents the real RAN components of the 5G network and the E2 Node.
- FlexRIC ( <https://openairinterface.org/mosaic5g/> ) : The Near Real-Time RAN Intelligent Controller that uses the E2 interface to connect with the E2 node. (more info on the E2 interface, the controller and the O-RAN architecture are found [here](https://bit.ly/3uOXuCV) and [here](https://www.o-ran.org/specifications)
- The xApp : the O-RAN component that is responsible for the data collection (uses FlexRIC SDK).



# OpenAirInterface
The build and the deployment processes are presented in details in the documentation found [here](https://gitlab.eurecom.fr/oai/openairinterface5g/-/blob/develop/doc/BUILD.md)

For the sake of simplicity, we just give the commands to build the version that is compatible with the MEP platform. 

We use a branch that is compatible with the MEP. Below the command to clone, checkout, install the prerequisites and build OAI:

``` bash
git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git oai
cd oai/
git checkout mep-compatible
cd cmake_targets
./build_oai -I -w USRP -i  #this will install some dependencies, and its done once
./build_oai  --gNB -c -C -w USRP --ninja
``` 

The build process will vary depending on the machine, these commands assume that you will use OAI with a USRP. 

To run OAI there are different config files found in `.../oai/dir/ci-scripts/conf_files` choses one depending on your use case. Run OAI with the command : 

```bash
cd ran_build/build
sudo ./nr-softmodem -O /path/to/config/file/oai-b210-remote-cn.conf --sa -E --continuous-tx
```
 OAI should be running. 


# FlexRIC 
We used FlexRIC release 1.0.0 deployed on the same machine as OAI (it can be deployed separately and connected remotely to OAI's gNB).
The build and deployment process are we documented on the official repo [here](https://gitlab.eurecom.fr/mosaic5g/flexric/-/blob/master/README.md?plain=0#flexric) and are straightforward.

After the compilation, and to test the proper connection between FlexRIC and OAI, FlexRIC needs to run first using the command : 
```bash
./build/examples/ric/nearRT-RIC
```
Then OAI on an other terminal : 
```bash
sudo ./nr-softmodem -O /path/to/config/file/oai-b210-remote-cn.conf --sa -E --continuous-tx
```
By now you should see logs similar to the figure bellow that indicates a new connection to the controller in FlexRIC's terminal.

Once the connection is tested, we can now run the xApp that will collect the RAN data using FlecRIC's SDK

# the xApp 

To prepare the setup environment just run the following command for `curl` 
```bash
sh -c "$(curl -fsSL https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/setup-xapp.sh)"
```
or If you want to use wget the following :
```bash
sh -c "$(wget https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/setup-xapp.sh -O -)"
```


Now you will need to install the python3 requirements from `path/to/flexric/build/examples/xApp/python3/requirements.txt` with the command : 
```bash
pip3 install -r requirements.txt
```

### config 
To run the xApp with flexric you will need to specify the database directory `db` and the database name that will be used by FlecRIC in the configuration file with the command : 
```
sudo nano /usr/local/etc/flexric/flexric.conf
```
The content should be something like :


```
[NEAR-RIC]
NEAR_RIC_IP = 127.0.0.1

[XAPP]
DB_PATH = /home/netsoft/oai/flexric/db/
DB_NAME = xapp_mep_db1
```

For the xApp configuration you will need to specify in `config.ini` file found in `.../flexric/build/examples/xApp/python3` the following : 

-  `RemoteRabbitmqAddress` : the ip address of RabbitMQ broker used with the MEP platform.
-  `RemoteRabbitmqAddress` : the port to communicate with the broker of the MEP platform.
-  `SQliteDBPath` : should be the same as the one in FlexRIC's config file `DB_PATH` key.
-  `SQliteDBName` : should be the same as the one in FlexRIC's config file `DB_NAME` key.


# Running all the components
Now that every thing is ready start by running FlexRIC 

Then the gNodeB of OAI 

and finally the xApp 


By now if a user is connected to the gNodeB, the xApp should be pulling data from FlexRIC's db parsing and pushing it to the broker.
