#!/bin/sh

DB_DIR="./db/"
SDK_FILE="ric_sdk.py"
BUILD_DIR="./build/"
PYTHON_FILE_PATH="./build/examples/xApp/python3/"
PYTHON_SDK_FILE_NAME="xapp_sdk.py"

#  check if the ./db/ dir exists 
if  [ -d $DB_DIR ]; then 
    echo "$DB_DIR exists"

else
    echo "$DB_DIR does not exist"
    echo "Creating $DB_DIR"
    mkdir $DB_DIR

fi 




#  check if the build dir exists and it contains the ric_sdk.py
#  check if the ./db/ dir exists 
if  [ -d $BUILD_DIR ]; then 
    echo -e "$BUILD_DIR exists \n Checking for flexric's python xapp sdk.."
    if [ ! -f $PYTHON_FILE_PATH$PYTHON_SDK_FILE_NAME ]; then 
        echo "$PYTHON_FILE_PATH$PYTHON_SDK_FILE_NAME does not exist"
        echo "Make sure you have built flexRIC release 1.0.0 correctly as the $PYTHON_SDK_FILE_NAME does not exist"
        exit 1
    else
        echo "$PYTHON_SDK_FILE_NAME found !"
    fi

else
    echo "$PYTHON_FILE_PATH$PYTHON_SDK_FILE_NAME does not exist"
    echo "Make sure you have built flexRIC release 1.0.0 correctly as the the build dir does not exist"
    exit 2

fi 



# clone the xApp and the config file in the ./build/example/python3

echo "cloning the xApp into $PYTHON_FILE_PATH"
RET=$(curl  -w "%{http_code}" -s -I https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/xapp-mep.py -o /dev/null)
curl  https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/xapp-mep.py -o $PYTHON_FILE_PATH/xapp_mep.py
if [ ! $RET -eq 200  ]; then 
    echo "Error curl-ing the code"
    exit 4 
fi

RET=$(curl  -w "%{http_code}" -s -I https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/xapp-mep.py -o /dev/null)
curl  https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/config.ini -o $PYTHON_FILE_PATH/config.ini
if [ ! $RET -eq 200  ]; then 
    echo "Error curl-ing the code"
    exit 5
fi

RET=$(curl  -w "%{http_code}" -s -I https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/xapp-mep.py -o /dev/null)
curl   https://gitlab.eurecom.fr/tlili/xapp-mep/-/raw/main/requirements.txt -o $PYTHON_FILE_PATH/requirements.txt
if [ ! $RET -eq 200  ]; then 
    echo "Error curl-ing the code"
    exit 6
fi

echo
echo

echo "All done you can now install the requirements from $PYTHON_FILE_PATH/requirements.txt using pip"
echo "And set your values in $PYTHON_FILE_PATH/config.ini then launch the xApp"