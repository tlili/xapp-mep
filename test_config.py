import configparser
import sys
import traceback

config_file_name = 'config.ini'
config = configparser.ConfigParser()
config.read(config_file_name)



try :
    remote_rabbitmq_ip = config['XAPP']['RemoteRabbitmqAddress']
    remote_rabbitmq_port = config['XAPP']['RemoteRabbitmqPort']
    sqlite_xapp_db_name = config['XAPP']['SQliteDBName']
    sqlite_xapp_db_path = config['XAPP']['SQliteDBPath']
except Exception as e: 
    traceback.print_exc()
    print("Error while reading configuration elements from ",config_file_name)
    sys.exit(2)


print(remote_rabbitmq_ip,remote_rabbitmq_port,sqlite_xapp_db_path+sqlite_xapp_db_name)
